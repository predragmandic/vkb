Virtual Keyboard for HP Prime calculators
===

I'm not the author of this code. I just took the liberty to put it in git since there are multiple people working on it.

Forum link: https://www.hpmuseum.org/forum/thread-8262-post-156173.html

Code guidelines
---

- Please use spaces as tabs. The only reason for this preference is because tab character is not currently recognized correctly when copying code directly to the calc emulator.

